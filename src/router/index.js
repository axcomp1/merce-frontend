import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Dashboard from '../views/Dashboard.vue';
import Producto from '../components/Producto.vue';
import Productos from '../components/Productos.vue';
import Carrito from '../components/Carrito.vue';

import Editar from '../views/Editar.vue';
import Nuevo from '../components/NuevoCliente.vue';
import NuevoUsuario from '../components/NuevoUsuario.vue';
 

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/editar/:id',
    name: 'Editar',
    component: Editar
  },
  {
    path: '/nuevo',
    name: 'Nuevo',
    component: Nuevo
  },
  {
    path: '/registrar',
    name: 'Registrar',
    component: NuevoUsuario
  },
  {
    path:'/productos',
    name: 'Productos',
    component: Productos
  },
  {
  path:'/producto/:id',
  component: Producto
},
{
path:'/carrito/:id',
component: Carrito
}
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
